# Dockerfile for Gitlab CI CD
#build maven environment
FROM maven:3.8.1-jdk-11 as builder
USER root
#create working directory
WORKDIR /builder
#add current folder to builder in maven image
ADD . /builder
#run command line create *.jar in target folder
RUN mvn install -DskipTests=true
#build java enviroment based on maven
FROM adoptopenjdk/openjdk11:alpine-jre
WORKDIR /app
#copy *.jar to builder that's maven enviroment
COPY  --from=builder /builder/target/audio-book-api-0.0.1-SNAPSHOT.jar .
#execute java *.jar file
CMD ["java","-jar","audio-book-api-0.0.1-SNAPSHOT.jar"]

## Dockerfile for self-build
#FROM adoptopenjdk/openjdk11:alpine-jre
#WORKDIR /app/android
#ARG JAR_FILE=target/audio-book-api-0.0.1-SNAPSHOT.jar
#COPY ${JAR_FILE} android.jar
#ENTRYPOINT ["java","-jar","android.jar"]


