cd ..
rm -r target
echo "Deleted target folder"
echo "------------------------------------------------------"
mvn install -DskipTest=true
# shellcheck disable=SC2164
cd target
java -jar audio-book-api-0.0.1-SNAPSHOT.jar > service.log 2>&1 &
echo "Completed build audio-book-api on local"
echo "------------------------------------------------------"