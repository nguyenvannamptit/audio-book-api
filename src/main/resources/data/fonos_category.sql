create table if not exists category
(
    id                 int auto_increment
        primary key,
    created_by         varchar(50)  not null,
    created_date       datetime(6)  null,
    last_modified_by   varchar(50)  null,
    last_modified_date datetime(6)  null,
    name               varchar(255) null,
    status             varchar(255) null
);

INSERT INTO fonos.category (id, created_by, created_date, last_modified_by, last_modified_date, name, status) VALUES (1, 'admin', '2021-05-29 16:02:13.000000', 'admin', '2021-05-29 16:02:13.000000', 'Sách nói miễn phí', '1');
INSERT INTO fonos.category (id, created_by, created_date, last_modified_by, last_modified_date, name, status) VALUES (2, 'admin', '2021-05-29 16:02:13.000001', 'admin', '2021-05-29 16:02:13.000001', 'Tư duy - Kỹ năng ', '1');
INSERT INTO fonos.category (id, created_by, created_date, last_modified_by, last_modified_date, name, status) VALUES (3, 'admin', '2021-05-29 16:02:13.000002', 'admin', '2021-05-29 16:02:13.000002', 'Tác giả Việt Nam ', '1');
INSERT INTO fonos.category (id, created_by, created_date, last_modified_by, last_modified_date, name, status) VALUES (4, 'admin', '2021-05-29 16:02:13.000003', 'admin', '2021-05-29 16:02:13.000003', 'Lãnh đạo', '1');
INSERT INTO fonos.category (id, created_by, created_date, last_modified_by, last_modified_date, name, status) VALUES (5, 'admin', '2021-05-29 16:02:13.000004', 'admin', '2021-05-29 16:02:13.000004', 'Tâm lý', '1');
INSERT INTO fonos.category (id, created_by, created_date, last_modified_by, last_modified_date, name, status) VALUES (6, 'admin', '2021-05-29 16:02:13.000005', 'admin', '2021-05-29 16:02:13.000005', 'Tác giả châu Á', '1');
INSERT INTO fonos.category (id, created_by, created_date, last_modified_by, last_modified_date, name, status) VALUES (7, 'admin', '2021-05-29 16:02:13.000006', 'admin', '2021-05-29 16:02:13.000006', 'Dành cho người trẻ', '1');
INSERT INTO fonos.category (id, created_by, created_date, last_modified_by, last_modified_date, name, status) VALUES (8, 'admin', '2021-05-29 16:02:13.000007', 'admin', '2021-05-29 16:02:13.000007', 'Sách kinh điển mọi thời đại', '1');
INSERT INTO fonos.category (id, created_by, created_date, last_modified_by, last_modified_date, name, status) VALUES (9, 'admin', '2021-05-29 16:02:13.000008', 'admin', '2021-05-29 16:02:13.000008', 'Sách khoa học viễn tưởng', '1');
INSERT INTO fonos.category (id, created_by, created_date, last_modified_by, last_modified_date, name, status) VALUES (10, 'admin', '2021-05-29 16:02:13.000009', 'admin', '2021-05-29 16:02:13.000009', 'Kinh điển giật gân', '1');