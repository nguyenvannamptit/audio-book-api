package com.namnguyen.audiobookapi.queue;

public interface MessagePublisher {
    void publish(final String message);
}
