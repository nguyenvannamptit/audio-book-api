package com.namnguyen.audiobookapi.controller;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.namnguyen.audiobookapi.common.response.ResponseData;
import com.namnguyen.audiobookapi.common.utils.MessageUtils;
import com.namnguyen.audiobookapi.common.utils.PaginationResult;
import com.namnguyen.audiobookapi.domain.dto.*;
import com.namnguyen.audiobookapi.domain.model.Book;
import com.namnguyen.audiobookapi.exception.CustomGlobalException;
import com.namnguyen.audiobookapi.repository.BookRepository;
import com.namnguyen.audiobookapi.service.interfaces.BookService;
import lombok.RequiredArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import javax.validation.Valid;
import java.io.IOException;
import java.util.List;
import java.util.Optional;

@RestController
@RequestMapping("")
@RequiredArgsConstructor
public class BookController {
    private final BookRepository bookRepo;
    private final BookService bookService;

    @GetMapping("/book/list")
    private List<Book> getHomePageBook() {
        return bookRepo.getHomePageBook();
    }

    @GetMapping("/book/trending")
    private List<Book> getTrendingBook() {
        return bookRepo.getTrendingBook();
    }

    @GetMapping("/book")
    private Optional<Book> getBook(@RequestParam("id") Integer id) {
        return bookRepo.findById(id);
    }

    @GetMapping("/search-by-name")
    private List<Book> findByName(@RequestParam("name") String name) {
        return bookRepo.searchByName(name);
    }

    @PostMapping(value = "/api/v1/admin/upload-file-s3", consumes = "multipart/form-data")
    private ResponseEntity<ResponseData> uploadBookToS3(
            @Valid @RequestParam("book") String book,
            @RequestPart("coverImage") MultipartFile coverImage,
            @RequestPart("faceBookImage") MultipartFile faceBookImage,
            @RequestPart("audioMp3") MultipartFile audioMp3)
            throws CustomGlobalException, IOException {
        ObjectMapper mapper = new ObjectMapper();
        UploadBookDto bookDto = mapper.readValue(book, UploadBookDto.class);
        UploadMediaBookDto mediaBook = UploadMediaBookDto.builder()
                .coverImage(coverImage)
                .faceBookImage(faceBookImage)
                .audioMp3(audioMp3)
                .build();
        bookService.uploadBookToS3(bookDto, mediaBook);
        return ResponseEntity.ok(ResponseData.ofSuccess(MessageUtils.getMessage("Thành công")));
    }


    @GetMapping("/api/v1/admin/book/list")
    private ResponseEntity<ResponseData> getAllBooks(
            @RequestParam(value = "offset", required = false) Integer offset,
            @RequestParam(value = "limit", required = false) Integer limit) {
        PaginationResult<BookDto> bookPage = bookService.getAllBooks(new PageDto(offset, limit));
        return ResponseEntity.ok(ResponseData.ofSuccess(MessageUtils.getMessage("Thành công"), bookPage));
    }

    @PutMapping("/api/v1/admin/book/delete")
    private ResponseEntity<ResponseData> deleteByUpdateStatus(@RequestParam(value = "bookId", required = false) String bookId) throws CustomGlobalException {
        bookService.deleteByUpdateStatus(bookId);
        return ResponseEntity.ok(ResponseData.ofSuccess(MessageUtils.getMessage("Thành công")));
    }

    @PutMapping("/api/v1/admin/book/update")
    private ResponseEntity<ResponseData> updateBook(@RequestParam("id") String id,
                                                    @RequestParam("title") String title,
                                                    @RequestParam("author") String author,
                                                    @RequestParam("intro") String intro,
                                                    @RequestParam("image") String image,
                                                    @RequestParam("coverImage") String coverImage,
                                                    @RequestParam("mp3") String mp3,
                                                    @RequestPart("imageFile") MultipartFile imageFile,
                                                    @RequestPart("coverImageFile") MultipartFile coverImageFile,
                                                    @RequestPart("mp3File") MultipartFile mp3File) throws CustomGlobalException {
        var bookDto = new BookDto();
        bookDto.setId(id);
        bookDto.setTitle(title);
        bookDto.setAuthor(author);
        bookDto.setIntro(intro);
        bookDto.setImage(image);
        bookDto.setCoverImage(coverImage);
        bookDto.setMp3(mp3);
        bookService.updateBook(bookDto, new BookFileDto(imageFile, coverImageFile, mp3File));
        return ResponseEntity.ok(ResponseData.ofSuccess(MessageUtils.getMessage("Thành công")));
    }

}
