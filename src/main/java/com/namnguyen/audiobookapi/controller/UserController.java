package com.namnguyen.audiobookapi.controller;

import com.namnguyen.audiobookapi.common.response.ResponseData;
import com.namnguyen.audiobookapi.common.utils.MessageUtils;
import com.namnguyen.audiobookapi.common.utils.PaginationResult;
import com.namnguyen.audiobookapi.domain.dto.*;
import com.namnguyen.audiobookapi.exception.CustomGlobalException;
import com.namnguyen.audiobookapi.service.interfaces.UserService;
import lombok.RequiredArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.HashMap;
import java.util.Map;

@RestController
@RequestMapping("")
@RequiredArgsConstructor
public class UserController {
    private final UserService userService;

    @PostMapping("/users/log-in")
    public ResponseEntity<ResponseData> LogInAccount(@Valid @RequestBody LoginFormDto request) throws CustomGlobalException {
        var accessToken = userService.logInAccount(request);
        return ResponseEntity.ok(ResponseData.ofSuccess(MessageUtils.getMessage("Thành công"), accessToken));
    }

    @PostMapping("/users/sign-up")
    public ResponseEntity<ResponseData> signUpAccount(@Valid @RequestBody SignUpDto request) throws CustomGlobalException {
        userService.signUpAccount(request);
        return ResponseEntity.ok(ResponseData.ofSuccess(MessageUtils.getMessage("Thành công")));
    }

    @PostMapping("/users/forget-password")
    public ResponseEntity<ResponseData> sendForgetEmail(@RequestParam("email") String email) throws CustomGlobalException {
        userService.sendForgetPasswordMail(email);
        return ResponseEntity.ok(ResponseData
                .ofSuccess(MessageUtils
                        .getMessage("Kiểm tra email để nhận mã khôi phục mật khẩu")));
    }

    @GetMapping("/users/reset-code")
    public ResponseEntity<ResponseData> getAllResetCodeOnRedis() {
        Map<Object, Object> data = userService.findAllResetCode();
        Map<String, String> map = new HashMap<>();
        for (Map.Entry<Object, Object> entry : data.entrySet()) {
            String key = (String) entry.getKey();
            map.put(key, data.get(key).toString());
        }
        return ResponseEntity.ok(ResponseData
                .ofSuccess(MessageUtils.getMessage("Thành công"), map));
    }

    @PutMapping("/api/v1/admin/users/delete")
    public ResponseEntity<ResponseData> deleteAccount(@RequestParam("id") String id) throws CustomGlobalException {
        userService.deleteAccount(id);
        return ResponseEntity.ok(ResponseData.ofSuccess(MessageUtils.getMessage("Thành công")));
    }

    @PutMapping("/users/update")
    public ResponseEntity<ResponseData> updateAccount(@Valid @RequestBody UpdateAccountDto request) throws CustomGlobalException {
        userService.updateAccount(request);
        return ResponseEntity.ok(ResponseData.ofSuccess(MessageUtils.getMessage("Thành công")));
    }

    @GetMapping("/api/v1/admin/users/list")
    public ResponseEntity<ResponseData> getAllUser(@RequestParam("offset") Integer offset,
                                                   @RequestParam("limit") Integer limit) {
        PaginationResult<UserDto> page = userService.getAllUser(new PageDto(offset, limit));
        return ResponseEntity.ok(ResponseData.ofSuccess(MessageUtils.getMessage("Thành công"), page));
    }
}
