package com.namnguyen.audiobookapi.controller;

import com.namnguyen.audiobookapi.common.response.ResponseData;
import com.namnguyen.audiobookapi.common.utils.MessageUtils;
import com.namnguyen.audiobookapi.domain.dto.CreateCategoryDto;
import com.namnguyen.audiobookapi.domain.dto.UpdateCategoryDto;
import com.namnguyen.audiobookapi.domain.model.Category;
import com.namnguyen.audiobookapi.exception.CustomGlobalException;
import com.namnguyen.audiobookapi.service.interfaces.CategoryService;
import lombok.RequiredArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;
import java.util.Optional;

@RestController
@RequestMapping("")
@RequiredArgsConstructor
public class CategoryController {
    private final CategoryService categoryService;

    // Mobile API
    @GetMapping("/category/list")
    public List<Category> getAllCategoryOnMobile() {
        return categoryService.getAllCategoryOnMobile();
    }

    @GetMapping("/category/book")
    public Optional<Category> getBookFromCategory(@RequestParam("id") Integer id) {
        return categoryService.getBookFromCategory(id);
    }

    // CMS API
    @GetMapping("/api/v1/category/list")
    public List<Category> getAllCategoryCMS() {
        return categoryService.getAllCategoryCMS();
    }

    @PostMapping("/api/v1/admin/category/create")
    public ResponseEntity<ResponseData> createCategory(@Valid @RequestBody CreateCategoryDto category) throws CustomGlobalException {
        categoryService.createCategory(category);
        return ResponseEntity.ok(ResponseData.ofSuccess(MessageUtils.getMessage("Thành công")));
    }

    @PutMapping("/api/v1/admin/category/delete")
    public ResponseEntity<ResponseData> deleteCategory(
            @RequestParam("id") String categoryId) throws CustomGlobalException {
        categoryService.deleteCategory(categoryId);
        return ResponseEntity.ok(ResponseData.ofSuccess(MessageUtils.getMessage("Thành công")));
    }

    @PutMapping("/api/v1/admin/category/update")
    public ResponseEntity<ResponseData> updateCategory(@Valid @RequestBody UpdateCategoryDto category) throws CustomGlobalException {
        categoryService.updateCategory(category);
        return ResponseEntity.ok(ResponseData.ofSuccess(MessageUtils.getMessage("Thành công")));
    }

}
