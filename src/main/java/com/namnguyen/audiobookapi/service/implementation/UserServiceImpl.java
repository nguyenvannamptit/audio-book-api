package com.namnguyen.audiobookapi.service.implementation;

import com.namnguyen.audiobookapi.common.utils.MessageUtils;
import com.namnguyen.audiobookapi.common.utils.ObjectMapperUtils;
import com.namnguyen.audiobookapi.common.utils.PaginationResult;
import com.namnguyen.audiobookapi.domain.dto.*;
import com.namnguyen.audiobookapi.domain.model.Book;
import com.namnguyen.audiobookapi.domain.model.User;
import com.namnguyen.audiobookapi.exception.CustomGlobalException;
import com.namnguyen.audiobookapi.repository.UserRepository;
import com.namnguyen.audiobookapi.security.jwt.TokenProvider;
import com.namnguyen.audiobookapi.service.interfaces.UserService;
import lombok.RequiredArgsConstructor;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.redis.core.HashOperations;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.mail.javamail.MimeMessageHelper;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;
import org.thymeleaf.TemplateEngine;
import org.thymeleaf.context.Context;

import javax.annotation.PostConstruct;
import javax.mail.*;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;
import java.util.Map;
import java.util.Optional;
import java.util.Properties;
import java.util.concurrent.ThreadLocalRandom;

import static com.namnguyen.audiobookapi.common.constants.Constants.*;
import static com.namnguyen.audiobookapi.common.constants.Constants.STATUS.ACTIVE;
import static com.namnguyen.audiobookapi.common.constants.Constants.STATUS.DEACTIVATE;
import static com.namnguyen.audiobookapi.security.AuthoritiesConstants.ADMIN;
import static com.namnguyen.audiobookapi.security.AuthoritiesConstants.CUSTOMER;


@Service
@RequiredArgsConstructor
public class UserServiceImpl implements UserService {
    private static final String KEY = "ResetCode";
    private final UserRepository userRepo;
    private final TokenProvider provider;
    private final AuthenticationManager manager;
    private final PasswordEncoder encoder;
    private final TemplateEngine templateEngine;
    private final RedisTemplate<String, String> redisTemplate;
    private HashOperations hashOperations;

    @PostConstruct
    private void init() {
        hashOperations = redisTemplate.opsForHash();
    }

    @Override
    public AccessTokenDto logInAccount(LoginFormDto request) throws CustomGlobalException {
        var authentication = manager.authenticate(
                new UsernamePasswordAuthenticationToken(request.getUsername(), request.getPassword()));
        var _user = userRepo.findByUsername(request.getUsername())
                .orElseThrow(() -> new CustomGlobalException(MessageUtils.getMessage("Người dùng không tồn tại")));
        SecurityContextHolder.getContext().setAuthentication(authentication);
        String accessToken = provider.generateToken(authentication);
        return AccessTokenDto.builder()
                .username(_user.getUsername())
                .accessToken(accessToken)
                .role(_user.getRole())
                .build();
    }

    @Override
    public void signUpAccount(SignUpDto request) throws CustomGlobalException {
        Optional<User> registerAccount = userRepo.findByUsername(request.getUsername());
        if (registerAccount.isPresent()) {
            throw new CustomGlobalException(MessageUtils.getMessage("Tài khoản không hợp lệ"));
        }
        var _user = User.builder()
                .username(request.getUsername())
                .password(encoder.encode(request.getPassword()))
                .status(ACTIVE)
                .build();
        switch (request.getRole()) {
            case ADMIN:
                _user.setRole(ADMIN);
                break;
            default:
                _user.setRole(CUSTOMER);
                break;
        }
        userRepo.save(_user);
    }

    @Override
    public void sendForgetPasswordMail(String email) throws CustomGlobalException {
        Properties props = new Properties();
        props.put("mail.smtp.auth", "true");
        props.put("mail.smtp.host", HOST_NAME);
        props.put("mail.smtp.socketFactory.port", SSL_PORT);
        props.put("mail.smtp.socketFactory.class", "javax.net.ssl.SSLSocketFactory");
        props.put("mail.smtp.port", SSL_PORT);
        props.put("mail.smtp.ssl.checkserveridentity", true);
        Session session = Session.getDefaultInstance(props, new javax.mail.Authenticator() {
            @Override
            protected PasswordAuthentication getPasswordAuthentication() {
                return new PasswordAuthentication(APP_EMAIL, APP_PASSWORD);
            }
        });
        if (email.equals("")) {
            throw new CustomGlobalException(MessageUtils.getMessage("Email không được để trống"));
        }
        User optionalUser = userRepo.findByEmail(email)
                .orElseThrow(()
                        -> new CustomGlobalException(
                        MessageUtils.getMessage("Email người dùng không tồn tại")));
        var resetCode = generateResetCode();
        var resetPassword = new ResetPasswordDto();
        resetPassword.setResetCode(generateResetCode());
        hashOperations.put(KEY, "resetCode", resetPassword.getResetCode());
        userRepo.save(optionalUser);
        sendForgetEmail(resetCode, session, email);
    }

    @Override
    public Map<Object, Object> findAllResetCode() {
        return hashOperations.entries(KEY);
    }

    @Override
    public void updateAccount(UpdateAccountDto account) throws CustomGlobalException {
        for (var user : userRepo.checkUpdateAccount(account.getUsername(), Integer.valueOf(account.getId()))) {
            if (user.getUsername().equals(account.getUsername())) {
                throw new CustomGlobalException(MessageUtils.getMessage("Tên người dùng đã tồn tại"));
            }
        }
        var updatedAccount = userRepo.findById(Integer.valueOf(account.getId()))
                .orElseThrow(
                        () -> new CustomGlobalException(MessageUtils.getMessage("Tài khoản không tồn tại")));
        updatedAccount.setUsername(account.getUsername());
        updatedAccount.setPassword(encoder.encode(account.getPassword()));
        userRepo.save(updatedAccount);
    }

    @Override
    public void deleteAccount(String id) throws CustomGlobalException {
        var deleteAccount = userRepo.findById(Integer.valueOf(id))
                .orElseThrow(
                        () -> new CustomGlobalException(MessageUtils.getMessage("Tài khoản không tồn tại")));
        if (deleteAccount.getStatus().equals(DEACTIVATE)) {
            throw new CustomGlobalException(
                    MessageUtils.getMessage("Tài khoản đang trong trạng thái không hoạt động"));
        } else {
            deleteAccount.setStatus(DEACTIVATE);
            userRepo.save(deleteAccount);
        }
    }

    @Override
    public PaginationResult<UserDto> getAllUser(PageDto pageDto) {
        Pageable pageable = PageRequest.of(pageDto.getOffset(), pageDto.getLimit());
        Page<User> userPage = userRepo.getAllUser(pageable);
        return this.getUsersPaginationResult(pageDto, userPage);
    }

    private PaginationResult<UserDto> getUsersPaginationResult(PageDto pageDto, Page<User> userPage) {
        PaginationResult<UserDto> data = new PaginationResult<>();
        data.setListRecord(ObjectMapperUtils.toDto(userPage.getContent(), UserDto.class));
        data.setTotalRecords(Math.toIntExact(userPage.getTotalElements()));
        data.setTotalPages(userPage.getTotalPages());
        data.setMaxResult(userPage.getSize());
        data.setFromRecordIndex(pageDto.getOffset());
        data.setCurrentPage(userPage.getNumber());
        return data;
    }

    private void sendForgetEmail(String resetCode, Session session, String email) {
        Context context = new Context();
        context.setVariable("title", "Forget your password");
        context.setVariable("link", resetCode);
        try {
            String body = templateEngine.process("ForgetPassword", context);
            MimeMessage message = new MimeMessage(session);
            message.setRecipients(Message.RecipientType.TO, InternetAddress.parse(email));
            MimeMessageHelper helper;
            helper = new MimeMessageHelper(message, true);
            helper.setTo(email);
            helper.setSubject("Email Reset Password");
            helper.setFrom(APP_EMAIL);
            helper.setText("", body);
            Transport.send(message);
        } catch (MessagingException e) {
            e.printStackTrace();
        }
    }

    public String generateResetCode() {
        var randomCode = ThreadLocalRandom.current().nextInt(Integer.parseInt("999999"));
        return String.format("%06d", randomCode);
    }
}
