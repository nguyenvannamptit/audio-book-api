package com.namnguyen.audiobookapi.service.implementation;

import com.namnguyen.audiobookapi.common.utils.CommonUtils;
import com.namnguyen.audiobookapi.common.utils.MessageUtils;
import com.namnguyen.audiobookapi.common.utils.ObjectMapperUtils;
import com.namnguyen.audiobookapi.domain.dto.CreateCategoryDto;
import com.namnguyen.audiobookapi.domain.dto.UpdateCategoryDto;
import com.namnguyen.audiobookapi.domain.model.Book;
import com.namnguyen.audiobookapi.domain.model.Category;
import com.namnguyen.audiobookapi.exception.CustomGlobalException;
import com.namnguyen.audiobookapi.repository.BookRepository;
import com.namnguyen.audiobookapi.repository.CategoryRepository;
import com.namnguyen.audiobookapi.service.interfaces.CategoryService;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.*;

import static com.namnguyen.audiobookapi.common.constants.Constants.STATUS.ACTIVE;
import static com.namnguyen.audiobookapi.common.constants.Constants.STATUS.DEACTIVATE;

@Service
@RequiredArgsConstructor
public class CategoryServiceImpl implements CategoryService {
    private final CategoryRepository categoryRepo;
    private final BookRepository bookRepo;

    @Override
    public List<Category> getAllCategoryOnMobile() {
        return categoryRepo.getCategoryOnMobile();
    }

    @Override
    public List<Category> getAllCategoryCMS() {
        return categoryRepo.findAll();
    }

    @Override
    public Optional<Category> getBookFromCategory(Integer id) {
        return categoryRepo.findById(id);
    }

    @Override
    public void createCategory(CreateCategoryDto createCategory) throws CustomGlobalException {
        if (categoryRepo.existsByCategoryName(createCategory.getCategoryName())) {
            throw new CustomGlobalException(MessageUtils.getMessage("Tên danh mục đã tồn tại"));
        } else {
            var category = ObjectMapperUtils.toEntity(createCategory, Category.class);
            category.setStatus(ACTIVE);
            categoryRepo.save(category);
        }
    }

    @Override
    public void deleteCategory(String id) throws CustomGlobalException {
        if (CommonUtils.isNullOrEmpty(id) && CommonUtils.isNumber(id)) {
            throw new CustomGlobalException(MessageUtils.getMessage("Mã danh mục không hợp lệ"));
        } else {
            var category = categoryRepo.findById(Integer.valueOf(id))
                    .orElseThrow(
                            () -> new CustomGlobalException(MessageUtils.getMessage("Danh mục không tồn tại")));
            if (category.getStatus().equals(DEACTIVATE)) {
                throw new CustomGlobalException(MessageUtils.getMessage("Danh mục đang không hoạt động"));
            } else {
                category.setStatus(DEACTIVATE);
                categoryRepo.save(category);
            }
        }

    }

    @Override
    public void updateCategory(UpdateCategoryDto categoryDto) throws CustomGlobalException {
        /**
         * Category has existed on database or not ?
         */
        var updatedCategory = categoryRepo.findById(Integer.valueOf(categoryDto.getCategoryId()))
                .orElseThrow(
                        () -> new CustomGlobalException(MessageUtils.getMessage("Danh mục không tồn tại")));
        /**
         * categoryName isn't duplicated on database
         */

        for (var category : categoryRepo.checkUpdateCategory(categoryDto.getCategoryName(),
                Integer.valueOf(categoryDto.getCategoryId()))) {
            if (category.getCategoryName().equals(categoryDto.getCategoryName())) {
                throw new CustomGlobalException(MessageUtils.getMessage("Tên danh mục đã tồn tại"));
            }
        }
        /**
         * Set updated data for category
         */
        updatedCategory.setStatus(categoryDto.getStatus());
        updatedCategory.setCategoryName(categoryDto.getCategoryName());
        categoryRepo.save(updatedCategory);
    }

//    private PaginationResult<BookDto> getBookPaginationResult(PageDto pageDto, Page<Book> bookPage) {
//        PaginationResult<BookDto> data = new PaginationResult<>();
//        data.setListRecord(ObjectMapperUtils.toDto(bookPage.getContent(), BookDto.class));
//        data.setTotalRecords(Math.toIntExact(bookPage.getTotalElements()));
//        data.setTotalPages(bookPage.getTotalPages());
//        data.setMaxResult(bookPage.getSize());
//        data.setFromRecordIndex(pageDto.getOffset());
//        data.setCurrentPage(bookPage.getNumber());
//        return data;
//    }
}
