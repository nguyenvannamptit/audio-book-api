package com.namnguyen.audiobookapi.service.implementation;


import com.namnguyen.audiobookapi.common.utils.*;
import com.namnguyen.audiobookapi.domain.dto.*;
import com.namnguyen.audiobookapi.domain.model.Book;
import com.namnguyen.audiobookapi.domain.model.Category;
import com.namnguyen.audiobookapi.exception.CustomGlobalException;
import com.namnguyen.audiobookapi.repository.BookRepository;
import com.namnguyen.audiobookapi.repository.CategoryRepository;
import com.namnguyen.audiobookapi.service.interfaces.BookService;
import lombok.RequiredArgsConstructor;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import java.io.IOException;

import static com.namnguyen.audiobookapi.common.constants.Constants.STATUS.*;


@Service
@RequiredArgsConstructor
public class BookServiceImpl implements BookService {
    private static final Logger LOGGER = LoggerFactory.getLogger(BookServiceImpl.class);
    private final BookRepository bookRepo;
    private final CategoryRepository cateRepo;
    private final AmazonClient s3Storage;

    @Override
    public void updateBook(BookDto updateBook, BookFileDto bookFileDto) throws CustomGlobalException {
        if (CommonUtils.isNullOrEmpty(updateBook.getId())
                || CommonUtils.isNumber(updateBook.getId())) {
            throw new CustomGlobalException(MessageUtils.getMessage("Mã sách không hợp lệ"));
        }
        Book book = bookRepo.findById(Integer.valueOf(updateBook.getId()))
                .orElseThrow(
                        () -> new CustomGlobalException(MessageUtils.getMessage("Mã sách không tồn tại")));
        validUpdatedBook(updateBook);
        Category category = cateRepo.findById(Integer.valueOf(updateBook.getCategoryId()))
                .orElseThrow(
                        () -> new CustomGlobalException(MessageUtils.getMessage("Mã danh mục không tồn tại")));
        book.setCategory(category);
        book.setTitle(updateBook.getTitle());
        book.setAuthor(updateBook.getAuthor());
        book.setIntro(updateBook.getIntro());
        book.setStatus(updateBook.getStatus());
        setBookFile(updateBook, bookFileDto, book);
    }

    private void validUpdatedBook(BookDto updateBook) throws CustomGlobalException {
        if (CommonUtils.isNullOrEmpty(updateBook.getTitle()) ||
                CommonUtils.isNullOrEmpty(updateBook.getAuthor()) ||
                CommonUtils.isNullOrEmpty(updateBook.getIntro()) ||
                CommonUtils.isNullOrEmpty(updateBook.getImage()) ||
                CommonUtils.isNullOrEmpty(updateBook.getCoverImage()) ||
                CommonUtils.isNullOrEmpty(updateBook.getMp3())) {
            throw new CustomGlobalException(MessageUtils.getMessage("Thông tin sách không được để trống"));
        }
        if (CommonUtils.maxLength(updateBook.getTitle(), 50) ||
                CommonUtils.maxLength(updateBook.getAuthor(), 50) ||
                CommonUtils.minLength(updateBook.getTitle(), 5) ||
                CommonUtils.minLength(updateBook.getAuthor(), 5)) {
            throw new CustomGlobalException(
                    MessageUtils.getMessage("Tên sách, Tác giả có độ dài từ 5 - 50 kí tự"));
        }
    }

    private void setBookFile(BookDto updateBook, BookFileDto bookFileDto, Book book) {
        if (!bookFileDto.getImage().isEmpty()) {
            book.setImage(updateBook.getImage());
        } else {
            book.setImage(s3Storage.uploadMultipartFile(bookFileDto.getImage()));
        }
        if (!bookFileDto.getCoverImage().isEmpty()) {
            book.setCoverImage(updateBook.getCoverImage());
        } else {
            book.setCoverImage(s3Storage.uploadMultipartFile(bookFileDto.getCoverImage()));
        }
        if (!bookFileDto.getMp3().isEmpty()) {
            book.setMp3(updateBook.getMp3());
        } else {
            book.setMp3(s3Storage.uploadMultipartFile(bookFileDto.getMp3()));
        }
    }

    @Override
    public void uploadBookToS3(@Validated UploadBookDto bookDto, UploadMediaBookDto media) throws CustomGlobalException {
        if (bookRepo.existsByTitle(bookDto.getTitle())) {
            throw new CustomGlobalException(MessageUtils.getMessage("Tên sách này đã tồn tại "));
        } else {
            Book uploadBook = ObjectMapperUtils.toEntity(bookDto, Book.class);
            Category category = cateRepo.findById(Integer.valueOf(bookDto.getCategoryId()))
                    .orElseThrow(() -> new CustomGlobalException(MessageUtils.getMessage("Danh mục sách không tồn tại")));
            String coverBook = s3Storage.uploadMultipartFile(media.getCoverImage());
            String faceBook = s3Storage.uploadMultipartFile(media.getFaceBookImage());
            String audio = s3Storage.uploadMultipartFile(media.getAudioMp3());
            uploadBook.setCategory(category);
            uploadBook.setCoverImage(coverBook);
            uploadBook.setImage(faceBook);
            uploadBook.setStatus(ACTIVE);
            uploadBook.setMp3(audio);
            bookRepo.save(uploadBook);
            LOGGER.info("UPLOAD FILE SUCCESSFUL");
        }
    }

    @Override
    public PaginationResult<BookDto> getAllBooks(PageDto pageDto) {
        Pageable pageable = PageRequest.of(pageDto.getOffset(), pageDto.getLimit());
        Page<Book> bookPage = bookRepo.getAllBooks(pageable);
        return this.getBookPaginationResult(pageDto, bookPage);
    }

    @Override
    public void deleteByUpdateStatus(String bookId) throws CustomGlobalException {
        Book book = bookRepo.findById(Integer.valueOf(bookId))
                .orElseThrow(() -> new CustomGlobalException(
                        MessageUtils.getMessage("Sách được lựa chọn không tồn tại")));
        if (book.getStatus().equals(DEACTIVATE)) {
            throw new CustomGlobalException(
                    MessageUtils.getMessage("Sách được lựa chọn đã trong trạng thái không tồn tại"));
        } else {
            book.setStatus(DEACTIVATE);
        }
        bookRepo.save(book);
    }

    private PaginationResult<BookDto> getBookPaginationResult(PageDto pageDto, Page<Book> bookPage) {
        PaginationResult<BookDto> data = new PaginationResult<>();
        data.setListRecord(ObjectMapperUtils.toDto(bookPage.getContent(), BookDto.class));
        data.setTotalRecords(Math.toIntExact(bookPage.getTotalElements()));
        data.setTotalPages(bookPage.getTotalPages());
        data.setMaxResult(bookPage.getSize());
        data.setFromRecordIndex(pageDto.getOffset());
        data.setCurrentPage(bookPage.getNumber());
        return data;
    }
}
