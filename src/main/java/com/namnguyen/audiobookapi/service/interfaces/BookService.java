package com.namnguyen.audiobookapi.service.interfaces;

import com.namnguyen.audiobookapi.common.utils.PaginationResult;
import com.namnguyen.audiobookapi.domain.dto.*;
import com.namnguyen.audiobookapi.exception.CustomGlobalException;
import org.springframework.validation.annotation.Validated;


public interface BookService {
    void updateBook(BookDto updateBook, BookFileDto bookFileDto) throws CustomGlobalException;

    void uploadBookToS3(@Validated UploadBookDto bookDto, UploadMediaBookDto media) throws CustomGlobalException;

    PaginationResult<BookDto> getAllBooks(PageDto pageDto);

    void deleteByUpdateStatus(String id) throws CustomGlobalException;
}
