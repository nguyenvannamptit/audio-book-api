package com.namnguyen.audiobookapi.service.interfaces;


import com.namnguyen.audiobookapi.common.utils.PaginationResult;
import com.namnguyen.audiobookapi.domain.dto.*;
import com.namnguyen.audiobookapi.exception.CustomGlobalException;

import java.util.Map;

public interface UserService {
    AccessTokenDto logInAccount(LoginFormDto request) throws CustomGlobalException;

    void signUpAccount(SignUpDto request) throws CustomGlobalException;

    void sendForgetPasswordMail(String email) throws CustomGlobalException;

    Map<Object, Object> findAllResetCode();

    void updateAccount(UpdateAccountDto account) throws CustomGlobalException;

    void deleteAccount(String id) throws CustomGlobalException;

    PaginationResult<UserDto> getAllUser(PageDto pageDto);
}
