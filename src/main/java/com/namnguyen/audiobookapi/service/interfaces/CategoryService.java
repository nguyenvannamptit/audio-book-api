package com.namnguyen.audiobookapi.service.interfaces;


import com.namnguyen.audiobookapi.domain.dto.CreateCategoryDto;
import com.namnguyen.audiobookapi.domain.dto.UpdateCategoryDto;
import com.namnguyen.audiobookapi.domain.model.Category;
import com.namnguyen.audiobookapi.exception.CustomGlobalException;

import java.util.List;
import java.util.Optional;

public interface CategoryService {
    List<Category> getAllCategoryOnMobile();

    List<Category> getAllCategoryCMS();

    Optional<Category> getBookFromCategory(Integer id);

    void createCategory(CreateCategoryDto createCategory) throws CustomGlobalException;

    void deleteCategory(String id) throws CustomGlobalException;

    void updateCategory(UpdateCategoryDto categoryDto) throws CustomGlobalException;

}
