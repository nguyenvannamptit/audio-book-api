package com.namnguyen.audiobookapi.exception;

public class CustomGlobalException extends Exception {
    public CustomGlobalException(String message) {
        super(message);
    }
}
