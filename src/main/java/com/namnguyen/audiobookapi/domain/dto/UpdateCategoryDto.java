package com.namnguyen.audiobookapi.domain.dto;

import lombok.*;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Positive;
import javax.validation.constraints.Size;
import java.io.Serializable;
import java.util.List;

@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
@Builder
public class UpdateCategoryDto implements Serializable {
    @NotBlank(message = "Mã danh mục không được bỏ trống")
    @Positive(message = "Mã danh mục không hợp lệ")
    private String categoryId;
    @NotBlank(message = "Tên danh mục không đựoc bỏ trống")
    @Size(min = 6, max = 50, message = "Độ dài của tên danh mục phải lớn hơn 6 và nhỏ hơn 50 kí tự ")
    private String categoryName;
    @NotBlank(message = "Trạng thái danh mục không đựoc bỏ trống")
    private String status;
}
