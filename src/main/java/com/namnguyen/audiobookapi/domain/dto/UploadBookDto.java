package com.namnguyen.audiobookapi.domain.dto;

import lombok.*;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Size;
import java.io.Serializable;

@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
@Builder
public class UploadBookDto implements Serializable {
    @NotBlank(message = "Danh mục sách không thể bỏ trống")
    private String categoryId;

    @NotBlank(message = "Tên sách không thể bỏ trống ")
    @Size(min = 6, max = 50, message = "Độ dài của tên sách phải lớn hơn 6 và nhỏ lơn 50 kí tự ")
    private String title;

    @NotBlank(message = "Tên tác giả không được bỏ trống ")
    @Size(min = 6, max = 50, message = "Độ dài của tên tác giả phải lớn hơn 6 và nhỏ lơn 50 kí tự ")
    private String author;

    @NotBlank(message = "Giới thiệu sách không được bỏ trống")
    @Size(min = 6, max = 1000000, message = "Độ dài của tên sách phải lớn hơn 6 và nhỏ lơn 50 kí tự ")
    private String intro;
}
