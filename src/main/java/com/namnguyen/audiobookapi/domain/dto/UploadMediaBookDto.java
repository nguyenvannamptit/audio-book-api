package com.namnguyen.audiobookapi.domain.dto;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.web.multipart.MultipartFile;

import java.io.Serializable;

@AllArgsConstructor
@NoArgsConstructor
@Data
@Builder
public class UploadMediaBookDto implements Serializable {
    private MultipartFile coverImage;
    private MultipartFile faceBookImage;
    private MultipartFile audioMp3;
}
