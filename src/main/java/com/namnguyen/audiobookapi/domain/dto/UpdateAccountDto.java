package com.namnguyen.audiobookapi.domain.dto;

import lombok.*;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Positive;
import java.io.Serializable;

@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
public class UpdateAccountDto extends SignUpDto implements Serializable {
    @NotBlank(message = "Mã tài khoản không được bỏ trống")
    @Positive(message = "Mã tài khoản không hợp lệ")
    private String id;
}
