package com.namnguyen.audiobookapi.domain.dto;

import lombok.*;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Pattern;
import javax.validation.constraints.Size;
import java.io.Serializable;

@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
@Builder
public class SignUpDto implements Serializable {
    @NotBlank(message = "Tên tài khoản không được để trống")
    @Size(min = 5,max = 20,message = "Tên tài khoản có độ dài từ 5 tới 20 kí tự")
    private String username;
    @NotBlank(message = "Mật khẩu không được để trống")
    @Pattern(regexp = "^(?=.*\\d)(?=.*[a-z])(?=.*[A-Z])(?=.*[a-zA-Z]).{8,}$",
            message = "Mật khẩu không hợp lệ")
    private String password;
    private String role;
}
