package com.namnguyen.audiobookapi.domain.dto;

import lombok.*;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Size;
import java.io.Serializable;

@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
@Builder
public class CreateCategoryDto implements Serializable {
    @NotBlank(message = "Tên danh mục không được bỏ trống")
    @Size(min = 6, max = 50, message = "Độ dài của tên danh mục phải lớn hơn 6 và nhỏ hơn 50 kí tự ")
    private String categoryName;
}
