package com.namnguyen.audiobookapi.domain.dto;

import lombok.*;

import java.io.Serializable;

@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
@Builder
public class AccessTokenDto implements Serializable {
    private String username;
    private String accessToken;
    private String role;
}
