package com.namnguyen.audiobookapi.domain.dto;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.io.Serializable;

@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
public class BookDto extends UploadBookDto implements Serializable {
    private String id;
    private String image;
    private String coverImage;
    private String mp3;
    private String status;
}
