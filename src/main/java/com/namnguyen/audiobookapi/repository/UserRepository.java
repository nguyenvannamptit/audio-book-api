package com.namnguyen.audiobookapi.repository;

import com.namnguyen.audiobookapi.domain.model.User;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.util.List;
import java.util.Optional;

public interface UserRepository extends JpaRepository<User, Integer> {
    @Query("SELECT s FROM User s " +
            "WHERE UPPER(s.username) = UPPER(:username) " +
            "AND (:accountId IS NULL OR s.id <> :accountId)")
    List<User> checkUpdateAccount(@Param("username") String username, @Param("accountId") Integer accountId);

    @Query("SELECT s FROM User s WHERE s.status = '1'")
    Page<User> getAllUser(Pageable pageable);

    Optional<User> findByUsername(String username);

    Optional<User> findByEmail(String email);
}
