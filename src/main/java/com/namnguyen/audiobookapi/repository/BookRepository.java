package com.namnguyen.audiobookapi.repository;

import com.namnguyen.audiobookapi.domain.model.Book;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.util.List;

public interface BookRepository extends JpaRepository<Book, Integer> {
    @Query("SELECT b FROM Book b WHERE b.status = '1'")
    List<Book> getHomePageBook();

    @Query("SELECT b FROM Book b")
    Page<Book> getAllBooks(Pageable pageable);

    @Query("SELECT b FROM Book b WHERE b.status = '2'")
    List<Book> getTrendingBook();

    @Query("SELECT b FROM Book b WHERE b.title LIKE CONCAT('%',:name,'%')")
    List<Book> searchByName(@Param("name") String name);

    boolean existsByTitle(String title);

}
