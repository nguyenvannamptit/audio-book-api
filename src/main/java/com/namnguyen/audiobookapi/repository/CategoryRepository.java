package com.namnguyen.audiobookapi.repository;

import com.namnguyen.audiobookapi.domain.model.Category;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.util.List;
import java.util.Optional;

/**
 * JPQL
 */
public interface CategoryRepository extends JpaRepository<Category, Integer> {
    @Query("SELECT s FROM Category s " +
            "WHERE UPPER(s.categoryName) = UPPER(:categoryName) " +
            "AND (:categoryId IS NULL OR s.id <> :categoryId)")
    List<Category> checkUpdateCategory(@Param("categoryName") String categoryName, @Param("categoryId") Integer categoryId);

    @Query("SELECT c FROM Category c WHERE c.status = '1'")
    List<Category> getCategoryOnMobile();

    boolean existsByCategoryName(String categoryName);
}
